/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Shape;

/**
 *
 * @author HP
 */
abstract class Shape {


abstract double area();
public abstract String toString();
abstract double perimeter();

}

class Circle1 extends Shape{
	double radius;
	public Circle1( double radius) {
		this.radius=radius;
	}
	public double getRadius() {
		return radius;
	}
		
 double area()
	{
			return Math.PI*radius*radius;
		}
	double  perimeter() {
	return 2*Math.PI*radius;	
	}
	
	public String toString() {
		return "Circle radius is= "+ getRadius()+ " and area is= "+area() + " and perimeter is= " +perimeter();
		
	}
}
class Square extends Shape{
	
	double side;
	public Square(double side ) {
		this.side=side;
	}
	public double getSide() {
		return side;
	}
		
 double area()
	{
			return side*side;
					}
	double  perimeter() {
	return 4*side;	
	}
	
	public String toString() {
		return "Square side is= "+ getSide()+ " and area= "+area() + "and perimeter is= " +perimeter();
		
	}
}

